var server = require("./server");   //別ファイルのserverをモジュールとして要求
var router = require("./router");   //別ファイルのrouterをモジュールとして要求
var requestHandlers = require("./requestHandlers");   //別ファイルのrequestHandlerをモジュールとして要求

var handle = {}   //関数"handle"を定義
handle["/"] = requestHandlers.start;    //handleの[ ]の中身が何もなかった時に実行するメソッドを指定
handle["/start"] = requestHandlers.start;   //handleの[ ]の中身が/startだった時に実行するメソッドを指定
handle["/upload"] = requestHandlers.upload;   //handleの[ ]の中身が/uploadだった時に実行するメソッドを指定
handle["/show"] = requestHandlers.show;   //handleの[ ]の中身が/showだった時に実行するメソッドを指定

server.start(router.route, handle);   //server.startを呼び出し、引数はrouter.routeの返り値とhandleの返り値
