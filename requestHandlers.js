var querystring = require("querystring"),   //Nodeのモジュールquerystringを要求
  fs = require("fs");   //Nodeのモジュールfsを要求、ファイルの内容を読み込んでNode.jsサーバに渡すモジュール
  formidable = require("formidable");   //Nodeのモジュールformidableを要求、受け取ったファイルデータの処理を抽象化するモジュール
  
/*
 * formidableモジュールを使用するには、モジュールのインストールが必要。
 * NPMというパッケージマネージャを使用して簡単にインストールが可能。ターミナルで下記を実行すればOK
 * npm install formidable
*/

function start(response){   //関数startの宣言
  console.log("Request handler 'start' was called.");   //コンソールにstartの要求があったことを出力

  var body = '<html>'+    //html文を変数bodyに代入(以下数行は同様)
  '<head>'+
  '<meta http-equiv="Content-Type"'+
  'content="text/html; charset=UTF-8"/>'+
  '</head>'+
  '<body>'+
  '<form action="/upload" enctype="multipart/form-data"'+
  'method="post">'+
  '<input type="file" name="upload" multiple="multiple">'+
  '<input type="submit" value="Uplpad file"/>'+
  '</form>'+
  '</body>'+
  '</html>';

  response.writeHead(200, {"Content-Type":"text/html"});    //responseのヘッダに記述
  response.write(body);   //responseのbodyに上記のhtml文が入ったbodyを記述
  response.end();   //responseの終了宣言
  }

function upload(response, request){   //関数uploadの宣言
  console.log("Request handler 'upload' was called.");    //uploadが呼ばれたことをコンソールに出力

  var form = new formidable.IncomingForm();   //formにformidableのIncomingFormメソッドを代入
  console.log("about to parse");    //これからパース(解析)することをコンソールに出力
  form.parse(request, function(error, fields, files){   //form(新しく作ったformidable.IncomingForm)をパース
    console.log("parsing done");    //パースが終わった旨をコンソールに出力

    /*Posible error on Windows systems:
    tried to rename to an already existig file*/
    fs.rename(files.upload.path, "/temp/test.jpg",function(err){    //fsのメソッドrenameを呼び出し
      if(err){
        fs.unlink("/temp/test.jpg");
        fs.rename(files.upload.path,"/temp/test.jpg");
      }
    });
    response.writeHead(200,{"Content-Type":"text/html"});
    response.write("received image:<br/>");
    response.write("<img src='/show'/>");
    response.end();
  });
}

function show(response){
  console.log("Request handler 'show' was called.");
  fs.readFile("/temp/test.jpg", "binary", function(error, file){
    if(error){
      response.writeHead(500, {"Content-Type":"text/plain"});
      response.write(error + "¥n");
      response.end();
    }else{
      response.writeHead(200, {"Content-Type":"image/png"});
      response.write(file, "binary");
      response.end();
    }
  });
}

exports.start = start;
exports.upload = upload;
exports.show = show;
