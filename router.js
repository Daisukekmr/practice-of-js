function route(handle, pathname, response, request) {   //index.jsの最後の行で呼ばれる関数(server.start)の引数
  console.log("About to route a request for " + pathname);    //これからどのpathnameでrouteするかをコンソールに出力
  if (typeof handle[pathname] === 'function') {   //handle[pathname]が関数の時実行する(有効なパスであればindex.jsで関数が代入されている)
    handle[pathname](response, request);    //その関数handleを呼び出し
  } else {    //handleが有効な関数じゃない時(つまり無効なパス名で来た時)
    console.log("No request handler found for " + pathname);    //コンソールに無効なパスでrequestが来た旨を出力
    response.writeHead(404,{"Content-Type":"text/plain"});    //responseのヘッダに404エラーを書く
    response.write("404 Not found");    //responseのbodyに記述
    response.end();   //responseの終了宣言
  }
}

exports.route = route;    //他のファイルからアクセスできるようrouteをexport
