var http = require("http");   //Nodeのhttpモジュールを要求
var url = require("url");   //Nodeのurlモジュールを要求

function start(route, handle) {   //index.jsの最後で呼ばれるメソッド
  function onRequest(request, response) {   //メソッドstartの引数となるメソッド
    var pathname = url.parse(request.url).pathname;   //引数pathnameにrequestされたURLのパス名を代入
    console.log("Request for " + pathname + " received.");    //requestを受信した旨をコンソールに出力
  route(handle, pathname, response, request);   //メソッドrouteに諸々の値を渡して呼び出し
  }

  http.createServer(onRequest).listen(3843, "27.120.91.137");    //サーバを立ててポート3843で待機
  console.log("Server has started.");   //サーバが立った旨をコンソールに出力
}

exports.start = start;    //他のファイルからアクセスできるようにexport
